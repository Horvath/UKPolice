//
//  UKPCrimesDataPresenter.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPCrimesDataPresenter: NSObject
{
    static let sharedInstance = UKPCrimesDataPresenter()

    private var networkManager = UKPNetworkManager()

    // MARK: - Crimes fetching
    func getCrimesAroundYou(latitude : Double,
                            longitude : Double,
                                date : String,
                           onSuccess : [UKPCrime] -> Void,
                             onError : NSError -> Void)
    {
        let parameters = ["lat" : String(format:"%f", latitude), "lng" : String(format:"%f", longitude)]

        networkManager.getFromUrl("https://data.police.uk/api/crimes-street/all-crime",
                                  parameters: parameters as! [String : String],
                                  completionInJson:{ (json : JSON) in
                                    onSuccess(self.getCrimes(json))
                                    }, error: onError)
    }

    // MARK: - Utils
    func getCrimes(json : JSON) -> [UKPCrime]
    {
        var crimes = [UKPCrime]()

        for item in json
        {
            let crime = UKPCrime.init(json: item.1)
            crimes.append(crime!)
        }

        return crimes
    }
}
