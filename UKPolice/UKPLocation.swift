//
//  UKPLocation.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPLocation: NSObject
{
    var name : String?
    var longitude : String?
    var postcode : String?
    var address : String?
    var latitude : String?
    var type : String?
    var descriptionText : String?

    // MARK: - Initialization
    init?(json : JSON)
    {
        super.init()

        name = json["name"].string
        longitude = json["longitude"].string
        postcode = json["postcode"].string
        address = json["address"].string
        latitude = json["latitude"].string
        type = json["type"].string
        descriptionText = json["description"].string
    }
}
