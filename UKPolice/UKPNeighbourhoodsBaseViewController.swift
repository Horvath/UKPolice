//
//  UKPNeighbourhoodsBaseViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit

class UKPNeighbourhoodsBaseViewController: UKPViewController
{
    var neighbourhoodsDataPresenter : UKPNeighbourhoodsDataPresenter!

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        neighbourhoodsDataPresenter = UKPNeighbourhoodsDataPresenter.sharedInstance
    }
}
