//
//  UKPCrimeLocation.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPCrimeLocation: NSObject
{
    var latitude : String?
    var longitude : String?
    var street : UKPStreet?

    init?(json : JSON)
    {
        super.init()

        latitude = json["latitude"].string
        longitude = json["longitude"].string
        street = UKPStreet.init(json: json["street"])
    }
}
