//
//  UKPCrimeDetailViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import MapKit

class UKPCrimeDetailViewController: UKPCrimesBaseViewController
{
    @IBOutlet weak var mapView: MKMapView!

    // MARK: - Data Source
    var crime : UKPCrime?

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        title = crime?.location?.street?.name
        mapView.showsUserLocation = true
        showCentrePoi()
    }

    // MARK: - Setups
    func showCentrePoi()
    {
        let centreCoord = CLLocationCoordinate2DMake(Double((crime?.location?.latitude!)!)!, Double((crime?.location?.longitude!)!)!)

        let centreAnnotation = MKPointAnnotation()
        centreAnnotation.coordinate = centreCoord
        mapView.addAnnotation(centreAnnotation)
        mapView.setCenterCoordinate(centreCoord, animated: true)

        let span = MKCoordinateSpanMake(0.0075, 0.0075)
        let region = MKCoordinateRegion(center:centreCoord, span: span)
        mapView.setRegion(region, animated: true)
    }
}
