//
//  UKPCrimesViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import MapKit

class UKPCrimesViewController:
    UKPCrimesBaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CLLocationManagerDelegate
{
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var crimesTableView: UITableView!

    var locationManager = CLLocationManager?()

    // MARK: - Data Source
    var crimes : [UKPCrime]?
    var filteredCrimes : [UKPCrime]?

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        presentLoading()
        setupLocationManager()
    }

    // MARK: - Setups
    func setupLocationManager()
    {
        locationManager = CLLocationManager()
        locationManager!.requestAlwaysAuthorization()

        locationManager!.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled()
        {
            locationManager!.delegate = self
            locationManager!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager!.startUpdatingLocation()
        }
    }

    // MARK: - Utils
    func fetchCrimes(latitude : Double, longitude : Double)
    {
        if crimes != nil
        {
            hideLoading()
            return
        }

        crimesDataPresenter.getCrimesAroundYou(latitude, longitude: longitude, date: "2013-01",
            onSuccess:
            { (crimes : [UKPCrime]) in
                self.hideLoading()
                if crimes.count == 0
                {
                    self.showWarningMessage("No crimes araund you!")
                }
                self.crimes = crimes
                self.crimesTableView.reloadData()
            })
            { (error : NSError) in
                self.hideLoading()
                self.handleError(error)
            }
    }

    func filterCrimes()
    {
        let searchPredicate = NSPredicate(format: "location.street.name CONTAINS[c] %@", searchBar.text!)
        let filteredArray = (crimes! as NSArray).filteredArrayUsingPredicate(searchPredicate)
        filteredCrimes = filteredArray as? [UKPCrime]
        crimesTableView.reloadData()
    }

    func emptyText(text : String) -> Bool
    {
        return text.stringByReplacingOccurrencesOfString(" ", withString: "") == ""
    }

    func navigateToCrimeDetails(crime : UKPCrime)
    {
        let crimesStoryboard = UIStoryboard.init(name: "Crimes", bundle: nil)

        let crimeDetailVC =
            crimesStoryboard.instantiateViewControllerWithIdentifier("UKPCrimeDetailViewController")
                as! UKPCrimeDetailViewController
        crimeDetailVC.crime = crime

        navigationController?.pushViewController(crimeDetailVC, animated: true)
    }

    // MARK: - TableView Delegate/DataSource methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if crimes == nil
        {
            return 0
        }

        return emptyText(searchBar.text!) ? (crimes?.count)! : (filteredCrimes?.count)!
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("crimeCell")!
        
        cell.textLabel?.text = emptyText(searchBar.text!)
            ? crimes![indexPath.row].location?.street?.name
            : filteredCrimes![indexPath.row].location?.street?.name

        cell.detailTextLabel?.text = emptyText(searchBar.text!)
            ? crimes![indexPath.row].category
            : filteredCrimes![indexPath.row].category

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        navigateToCrimeDetails(crimes![indexPath.row])
    }

    // MARK: - UISearchBar Delegate methods
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if emptyText(searchText)
        {
            crimesTableView.reloadData()
            return
        }
        filterCrimes()
    }

    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        if emptyText(searchBar.text!)
        {
            crimesTableView.reloadData()
            return
        }
        filterCrimes()
    }

    // MARK: - Location manager delegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation : CLLocation = locations[0] 
        let lat = userLocation.coordinate.latitude;
        let long = userLocation.coordinate.longitude;

        fetchCrimes(lat, longitude: long)
    }
}
