//
//  UKPNeighbourhoodDetail.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPNeighbourhoodDetail: NSObject
{
    var urlForce : String?
    var contactDetails : UKPContactDetails?
    var name : String?
    var links : [UKPLink]?
    var centre : UKPCoordinate?
    var locations : [UKPLocation]?
    var descriptionText: String?
    var neighbourhoodDetailId: String?
    var population: String?

    // MARK: - Initialization
    init?(json : JSON)
    {
        super.init()

        urlForce = json["url_force"].string;
        contactDetails = UKPContactDetails.init(json: json["contact_details"])
        name = json["name"].string;
        links = getLinks(json["links"])
        centre = UKPCoordinate.init(json: json["centre"])
        self.locations = getLocations(json["locations"])
        descriptionText = json["description"].string
        neighbourhoodDetailId = json["id"].string
        population = json["population"].string
    }

    // MARK: - Utils
    func getLinks(json : JSON) -> [UKPLink]
    {
        var links = [UKPLink]()

        for item in json
        {
            links.append(UKPLink.init(json: item.1)!)
        }

        return links
    }

    func getLocations(json : JSON) -> [UKPLocation]
    {
        var locations = [UKPLocation]()

        for item in json
        {
            locations.append(UKPLocation.init(json: item.1)!)
        }

        return locations
    }
}
