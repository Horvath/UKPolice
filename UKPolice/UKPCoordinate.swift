//
//  UKPCoordinate.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPCoordinate: NSObject
{
    var latitude : String?
    var longitude : String?

    // MARK: - Initialization
    init?(json : JSON)
    {
        super.init()

        latitude = json["latitude"].string
        longitude = json["longitude"].string
    }
}
