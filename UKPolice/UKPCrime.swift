//
//  UKPCrime.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPCrime: NSObject
{
    var category : String?
    var persistentId : String?
    var locationType : String?
    var locationSubType : String?
    var locationId : String?
    var location : UKPCrimeLocation?
    var context : String?
    var month : String?
    var outcomeStatus : UKPOutcomeStatus?

    init?(json : JSON)
    {
        super.init()

        category = json["category"].string
        persistentId = json["persistent_id"].string
        locationType = json["location_type"].string
        locationSubType = json["location_subtype"].string
        locationId = json["id"].string
        location = UKPCrimeLocation.init(json: json["location"])
        context = json["context"].string
        month = json["month"].string
        outcomeStatus = UKPOutcomeStatus.init(json: json["outcome_status"])
    }
}
