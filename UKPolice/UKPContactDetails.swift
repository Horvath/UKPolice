//
//  UKPContactDetails.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPContactDetails: NSObject
{
    var twitter : String?
    var facebook : String?
    var telephone : String?
    var email : String?

    // MARK: - Initialization
    init?(json : JSON)
    {
        super.init()

        twitter = json["twitter"].string
        facebook = json["facebook"].string
        telephone = json["telephone"].string
        email = json["email"].string
    }
}
