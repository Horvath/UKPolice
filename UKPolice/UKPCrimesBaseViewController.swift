//
//  UKPCrimesBaseViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit

class UKPCrimesBaseViewController: UKPViewController
{
    var crimesDataPresenter : UKPCrimesDataPresenter!

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        crimesDataPresenter = UKPCrimesDataPresenter.sharedInstance
    }
}
