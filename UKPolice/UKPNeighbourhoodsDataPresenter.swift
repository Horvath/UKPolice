//
//  UKPNeighbourhoodsDataPresenter.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPNeighbourhoodsDataPresenter: NSObject
{
    static let sharedInstance = UKPNeighbourhoodsDataPresenter()

    private var networkManager = UKPNetworkManager()

    // MARK: - Neighbourhood fetching
    func getNeighbourhoods(onSuccess: [UKPNeighbourhood] -> Void,
                           onError: NSError -> Void)
    {
        networkManager.getFromUrl("https://data.police.uk/api/leicestershire/neighbourhoods",
            completionInJson:
            { (json : JSON) in
                    onSuccess(self.getNeighbourhoodsFromJson(json))
            }, error: onError)
    }

    func getNeighbourhoodDetail(neighboourhoodId : String,
                                onSuccess: (UKPNeighbourhoodDetail) -> Void,
                                onError: NSError -> Void)
    {
        networkManager.getFromUrl("https://data.police.uk/api/leicestershire/\(neighboourhoodId)",
            completionInJson:
            { (json : JSON) in
                    onSuccess(UKPNeighbourhoodDetail.init(json: json)!)
            }, error: onError)
    }

    // MARK: - Utils
    func getNeighbourhoodsFromJson(json : JSON) -> [UKPNeighbourhood]
    {
        var neighbourhoods = [UKPNeighbourhood]()

        for item in json
        {
            let neighbourhood = UKPNeighbourhood.init(json: item.1)
            neighbourhoods.append(neighbourhood!)
        }

        return neighbourhoods
    }
}
