//
//  UKPNeighbourhoodShowCentreViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import MapKit

class UKPNeighbourhoodShowCentreViewController: UKPNeighbourhoodsBaseViewController, MKMapViewDelegate
{
    var centreCoordinate : UKPCoordinate!

    @IBOutlet weak var mapView: MKMapView!

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        showCentrePoi()
    }

    // MARK: - Setups
    func showCentrePoi()
    {
        let centreCoord = CLLocationCoordinate2DMake(Double(centreCoordinate.latitude!)!, Double(centreCoordinate.longitude!)!)
        let centreAnnotation = MKPointAnnotation()
        centreAnnotation.coordinate = centreCoord
        mapView.addAnnotation(centreAnnotation)
        mapView.setCenterCoordinate(centreCoord, animated: true)
    }

    // MARK: - Actions
    @IBAction func closeClicked(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
