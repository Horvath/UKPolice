//
//  UKPNeighbourhoodDetailViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit

class UKPNeighbourhoodDetailViewController:
    UKPNeighbourhoodsBaseViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var forceUrlButton: UIButton!
    @IBOutlet weak var faceBookLink: UILabel!
    @IBOutlet weak var twitterLink: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var linksTableView: UITableView!
    @IBOutlet weak var locationsTableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!

    // MARK: - DataSource
    var neighbourhood : UKPNeighbourhood?
    var neighbourhoodDetail : UKPNeighbourhoodDetail?

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        fetchDetailData()
    }

    // MARK: - Utils
    func fetchDetailData()
    {
        presentLoading()

        neighbourhoodsDataPresenter.getNeighbourhoodDetail((neighbourhood?.neighbourhoodId)!,
            onSuccess:
        { (neighbourhoodDetail : UKPNeighbourhoodDetail) in
            self.neighbourhoodDetail = neighbourhoodDetail
            self.refreshAppearance()
            self.hideLoading()
        })
        { (error: NSError) in
            self.hideLoading()
            self.handleError(error)
        }
    }

    func refreshAppearance()
    {
        self.titleLabel.text = neighbourhoodDetail?.name
        self.forceUrlButton.setTitle(neighbourhoodDetail?.urlForce, forState: .Normal)
        self.faceBookLink.text = neighbourhoodDetail?.contactDetails?.facebook
        self.twitterLink.text = neighbourhoodDetail?.contactDetails?.twitter
        self.phoneNumber.text = neighbourhoodDetail?.contactDetails?.telephone
        self.email.text = neighbourhoodDetail?.contactDetails?.email
        self.descriptionLabel.text = neighbourhoodDetail?.descriptionText

        locationsTableView.reloadData()
        linksTableView.reloadData()
    }

    func openUrl(url : String)
    {
        UIApplication.sharedApplication().openURL(NSURL.init(string: url)!)
    }

    // MARK: - TableView Delegate/DataSource methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (neighbourhoodDetail == nil)
        {
            return 0
        }

        return (tableView == linksTableView
            ? neighbourhoodDetail?.links?.count
            : neighbourhoodDetail?.locations?.count)!
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("detailCell")!

        cell.textLabel?.text = tableView == linksTableView
            ? neighbourhoodDetail?.links![indexPath.row].title
            : neighbourhoodDetail?.locations![indexPath.row].name
        cell.detailTextLabel?.text = tableView == linksTableView
            ? neighbourhoodDetail?.links![indexPath.row].url
            : neighbourhoodDetail?.locations![indexPath.row].address

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        if tableView == linksTableView
        {
            openUrl((neighbourhoodDetail?.links![indexPath.row].url)!)
        }
    }

    // MARK: - Actions
    @IBAction func forceUrlClicked(sender: AnyObject)
    {
        openUrl((neighbourhoodDetail?.urlForce)!)
    }

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "NeighbourhoodCentre"
        {
            let destVC = segue.destinationViewController as! UKPNeighbourhoodShowCentreViewController
            destVC.centreCoordinate = neighbourhoodDetail?.centre
        }
    }

}
