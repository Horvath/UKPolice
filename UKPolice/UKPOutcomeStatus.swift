//
//  UKPOutcomeStatus.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPOutcomeStatus: NSObject
{
    var category : String?
    var date : String?

    init?(json : JSON)
    {
        super.init()

        category = json["category"].string
        date = json["date"].string
    }
}
