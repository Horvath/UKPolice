//
//  UKPViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import ALLoadingView

class UKPViewController: UIViewController
{
    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    // MARK: - Full Screen Loading
    func presentLoading()
    {
        ALLoadingView.manager.bluredBackground = true

        ALLoadingView.manager.showLoadingViewOfType(.MessageWithIndicator, windowMode: .Fullscreen, completionBlock: nil)
    }

    func hideLoading()
    {
        ALLoadingView.manager.hideLoadingViewWithDelay(0.4)
    }

    // MARK: - Alert showing
    func handleError(error : NSError)
    {
        let alert = UIAlertController.init(title: "ERROR!", message: error.localizedDescription, preferredStyle: .Alert)
        let okAction = UIAlertAction.init(title: "OK", style: .Default, handler: nil)

        alert.addAction(okAction)

        presentViewController(alert, animated: true, completion: nil)
    }

    func showWarningMessage(message : String)
    {
        let alert = UIAlertController.init(title: "ERROR!", message: message, preferredStyle: .Alert)
        let okAction = UIAlertAction.init(title: "OK", style: .Default, handler: nil)

        alert.addAction(okAction)

        presentViewController(alert, animated: true, completion: nil)
    }

    // MARK: - Appearance
    func hideNavigationBar()
    {
        navigationController?.navigationBarHidden = true
    }
}
