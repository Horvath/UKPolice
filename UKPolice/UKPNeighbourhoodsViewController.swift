//
//  UKPNeighbourhoodsViewController.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit

class UKPNeighbourhoodsViewController:
    UKPNeighbourhoodsBaseViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var neighbourhoodsTableView: UITableView!

    // MARK: - DataSource
    var neighbourhoods : [UKPNeighbourhood]?

    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        setupAppearance()
        fetchNeighbourhoods()
    }

    // MARK: - Setups
    func setupAppearance()
    {
        neighbourhoods = [UKPNeighbourhood]()
    }

    // MARK: - Utils
    func fetchNeighbourhoods()
    {
        presentLoading()
        neighbourhoodsDataPresenter.getNeighbourhoods(
        { (neighbourhoods : [UKPNeighbourhood]) in
            self.hideLoading()
            self.neighbourhoods = neighbourhoods
            self.neighbourhoodsTableView.reloadData()
        })
        { (error: NSError) in
            self.hideLoading()
            self.handleError(error)
        }
    }

    // MARK: - TableView delegate/datasource methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (neighbourhoods?.count)!
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let neighbourhoodCell = tableView.dequeueReusableCellWithIdentifier("NeighbourhoodCell")!
        neighbourhoodCell.textLabel?.text = neighbourhoods![indexPath.row].neighbourhoodName
        return neighbourhoodCell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        navigateToNeighbourhoodDetail(neighbourhoods![indexPath.row])
    }

    // MARK: - Navigation
    func navigateToNeighbourhoodDetail(neighbourhood : UKPNeighbourhood)
    {
        let neighbourhoodsStoryboard = UIStoryboard.init(name: "Neighbourhoods", bundle: nil)

        let neighbourhoodDetailViewController =
            neighbourhoodsStoryboard.instantiateViewControllerWithIdentifier("UKPNeighbourhoodDetailViewController")
            as! UKPNeighbourhoodDetailViewController

        neighbourhoodDetailViewController.neighbourhood = neighbourhood;

        navigationController?.pushViewController(neighbourhoodDetailViewController, animated: true)
    }
}
