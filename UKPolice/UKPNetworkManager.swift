//
//  UKPNetworkManager.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UKPNetworkManager: NSObject
{
    // MARK: - GET
    func getFromUrl(url : String,
                    completionInJson : (JSON) -> Void,
                    error : NSError -> Void)
    {
        Alamofire.request(.GET, url).responseJSON { (response : Response) in
            if (response.result.error != nil)
            {
                error(response.result.error!)
                return
            }

            let json = JSON(data: response.data!)
            print(json)
            print(Request)
            completionInJson(json)
        }
    }

    func getFromUrl(url : String,
                    parameters : [String : String],
                    completionInJson : (JSON) -> Void,
                    error : NSError -> Void)
    {
        Alamofire.request(.GET, url, parameters: parameters, encoding: .URL).responseJSON { (response : Response) in
            if (response.result.error != nil)
            {
                error(response.result.error!)
                return
            }

            let json = JSON(data: response.data!)
            print(json)
            completionInJson(json)
        }
    }
}
