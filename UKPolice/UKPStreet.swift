//
//  UKPStreet.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 10. 01..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPStreet: NSObject
{
    var streetId : String?
    var name : String?

    init?(json : JSON)
    {
        super.init()

        streetId = json["id"].string
        name = json["name"].string
    }
}
