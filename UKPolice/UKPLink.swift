//
//  UKPLink.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPLink: NSObject
{
    var url : String?
    var descriptionText : String?
    var title : String?

    // MARK: - Initialization
    init?(json : JSON)
    {
        super.init()

        url = json["url"].string
        descriptionText = json["description"].string
        title = json["title"].string
    }
}
