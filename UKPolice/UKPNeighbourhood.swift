//
//  UKPNeighbourhood.swift
//  UKPolice
//
//  Created by Horváth Dávid on 2016. 09. 30..
//  Copyright © 2016. davidhrvth. All rights reserved.
//

import UIKit
import SwiftyJSON

class UKPNeighbourhood: NSObject
{
    var neighbourhoodId : String?
    var neighbourhoodName : String?

    // MARK: - Initialization
    init?(json : JSON)
    {
        super.init()

        neighbourhoodId = json["id"].string
        neighbourhoodName = json["name"].string
    }
}
